#!/bin/bash
while true
do
  echo "==============================="
  echo 
  echo "Menu----------                 "
  echo 
  echo "-Made-by-Guilherme_Silva-------"
  echo "==============================="
  echo "1 - Run Normal Mode: "
  echo "2 - Run Ramdom Mode: "
  echo "3 - Run Loop Mode: "
  echo "4 - See How to get Coverage Number: "
  echo "5 - Student Number"
  echo "Enter q to exit: "
  echo 
  echo "Waiting for your selection:"
  read answer
  case "$answer" in
    1) read -p "Enter an Position " Position
       bash ReadN.sh $Position ;;
    2) read -p "How Many Runs (>=0) ? " nRuns 
       i=0 
       read -p "Name your .txt: " a
       while [ $i -lt $nRuns ]
       do
         r=$(( ( RANDOM % 48502 )  + 1 ))
         bash ReadN.sh $r >> "$a".txt
         i=$(( $i + 1 ))
       done 
       ;; 
     3) i=0
        read -p "Name your .txt: " n
        while [ $i -lt 5 ]
        do
          read -p "To make the text File Enter 5 Different Positions: " r
          bash ReadN.sh $r >> "$n".txt
        i=$(( i + 1 ))
        done ;;
    4) cat ReadN.sh ;;
    5) bash ReadN.sh 0178 ;;
    6) echo "May the Force be with you" ;;
    q) break ;;
  esac
done  
