.:samtools coverage  assembly.bam

  Output:

  #rname	   startpos	   endpos	numreads	covbases	coverage	meandepth	meanbaseq	meanmapq
gi|9626243|ref|NC_001416.1|	1	48502	9404	48485	99.9649	21.4393	16.8	41

  Mostra o numreads de toda a sequência (9404)

.:samtools coverage  assembly.bam -r "gi|9626243|ref|NC_001416.1|:178-178"
 
 Output: numreads 23

  1 - É Filtrado numa região específica pelo parâmetro -r

  2 - Para filtrar é necessário o #rname (gi|9626243|ref|NC_001416.1|) 

  3 - E o máximo e mínimo da sequencia (Neste caso como é só um valor fica 178-178) 

.: | awk '{ print $4 }'
   O awk serve para apresentar uma coluna (neste caso a coluna 4: numreads 23)

.:samtools coverage  assembly.bam -r "gi|9626243|ref|NC_001416.1|:$1-$1" | awk '{ print $4 }'.:
  Input:    Script._Name.sh 178
  Output: numreads 23 
